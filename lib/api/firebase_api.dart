import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:gorev_takip_uygulamasi/model/gorev.dart';
import 'package:gorev_takip_uygulamasi/yardimci.dart';

class FirebaseApi {
  static Future<String> olustur(Gorev gorev) async {
    print("olustur");
    final gorevDokuman = FirebaseFirestore.instance.collection('gorev').doc();
 print("olustur 1");
    gorev.kimlik = gorevDokuman.id;
     print("olustur 3");
     await gorevDokuman.set(gorev.toJson());
     print("olustur 2");

    return gorevDokuman.id;
  }

  static Stream<List<Gorev>> gorevlerGetir() => FirebaseFirestore.instance
      .collection('gorev')
      .orderBy(GorevOzellikler.createdTime, descending: true)
      .snapshots()
      .transform(Utils.transformer(Gorev.fromJson));

  static Future guncelle(Gorev gorev) async {
    final gorevDokuman =
        FirebaseFirestore.instance.collection('gorev').doc(gorev.kimlik);

    await gorevDokuman.update(gorev.toJson());
  }

  static Future sil(Gorev gorev) async {
    final dokuman =
        FirebaseFirestore.instance.collection('gorev').doc(gorev.kimlik);

    await dokuman.delete();
  }
}

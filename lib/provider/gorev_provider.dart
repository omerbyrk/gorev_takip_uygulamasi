import 'package:flutter/cupertino.dart';
import 'package:gorev_takip_uygulamasi/api/firebase_api.dart';
import 'package:gorev_takip_uygulamasi/model/gorev.dart';

class GorevProvider extends ChangeNotifier {
  List<Gorev> _gorevler = [];

  List<Gorev> get gorevler => _gorevler.where((gorev) => gorev.yapildiMi == false).toList();

  List<Gorev> get yapilanGorevler =>
      _gorevler.where((gorev) => gorev.yapildiMi == true).toList();

  void gorevAyarla(List<Gorev> gorevler) =>
      WidgetsBinding.instance.addPostFrameCallback((_) {
        _gorevler = gorevler;
        notifyListeners();
      });

  void gorevEkle(Gorev gorev) => FirebaseApi.olustur(gorev);

  bool gorevDurumDegistir(Gorev gorev) {
    gorev.yapildiMi = !gorev.yapildiMi;
    FirebaseApi.guncelle(gorev);

    return gorev.yapildiMi;
  }

  void gorevSil(Gorev gorev) => FirebaseApi.sil(gorev);

  void gorevGuncelle(Gorev gorev, String baslik, String tanim) {
    gorev.baslik = baslik;
    gorev.tanim = tanim;

    FirebaseApi.guncelle(gorev);
  }
}

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:gorev_takip_uygulamasi/page/gorev_listeleme.dart';
import 'package:gorev_takip_uygulamasi/provider/gorev_provider.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  static final String baslik = 'Görev Takip Uygulaması';

  @override
  Widget build(BuildContext context) => ChangeNotifierProvider(
        create: (context) => GorevProvider(),
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: baslik,
          theme: ThemeData(
            primarySwatch: Colors.blue,
            scaffoldBackgroundColor: Color(0xFFf6f5ee),
          ),
          home: HomePage(),
        ),
      );
}

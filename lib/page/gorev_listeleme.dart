import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:gorev_takip_uygulamasi/api/firebase_api.dart';
import 'package:gorev_takip_uygulamasi/model/gorev.dart';
import 'package:gorev_takip_uygulamasi/provider/gorev_provider.dart';
import 'package:gorev_takip_uygulamasi/widget/gorev_diyalog_ekle_widget.dart';
import 'package:gorev_takip_uygulamasi/widget/tamamlanan_liste_widget.dart';
import 'package:gorev_takip_uygulamasi/widget/gorev_liste_widget.dart';

import '../main.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int secilenIndex = 0;

  @override
  Widget build(BuildContext context) {
    final sekmeler = [
      GorevListesiWidget(),
      YapilanGorevListesiWidget(),
    ];

    return Scaffold(
      appBar: AppBar(
        title: Text(MyApp.baslik),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Theme.of(context).primaryColor,
        unselectedItemColor: Colors.white.withOpacity(0.7),
        selectedItemColor: Colors.white,
        currentIndex: secilenIndex,
        onTap: (index) => setState(() {
          secilenIndex = index;
        }),
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.fact_check_outlined),
            label: 'Yapılacak görevler',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.done, size: 28),
            label: 'Yapılan görevler',
          ),
        ],
      ),
      body: StreamBuilder<List<Gorev>>(
        stream: FirebaseApi.gorevlerGetir(),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return Center(child: CircularProgressIndicator());
            default:
              if (snapshot.hasError) {
                return buildText('Bişeyler yanlış gitti');
              } else {
                final gorevler = snapshot.data;

                final provider = Provider.of<GorevProvider>(context);
                provider.gorevAyarla(gorevler);

                return sekmeler[secilenIndex];
              }
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        shape: CircleBorder(
        ),
        backgroundColor: Colors.blue,
        onPressed: () => showDialog(
          context: context,
          builder: (context) => GorevDiyalogEkleWidget(),
          barrierDismissible: false,
        ),
        child: Icon(Icons.add),
      ),
    );
  }
}

Widget buildText(String text) => Center(
      child: Text(
        text,
        style: TextStyle(fontSize: 24, color: Colors.white),
      ),
    );

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:gorev_takip_uygulamasi/model/gorev.dart';
import 'package:gorev_takip_uygulamasi/provider/gorev_provider.dart';
import 'package:gorev_takip_uygulamasi/widget/gorev_form_widget.dart';

class GorevGuncellemePage extends StatefulWidget {
  final Gorev gorev;

  const GorevGuncellemePage({Key key, @required this.gorev}) : super(key: key);

  @override
  _GorevGuncellemePageState createState() => _GorevGuncellemePageState();
}

class _GorevGuncellemePageState extends State<GorevGuncellemePage> {
  final _formKey = GlobalKey<FormState>();

  String baslik;
  String tanim;

  @override
  void initState() {
    super.initState();

    baslik = widget.gorev.baslik;
    tanim = widget.gorev.tanim;
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text('Görev Güncelleme'),
          actions: [
            IconButton(
              icon: Icon(Icons.delete_forever),
              onPressed: () {
                final provider =
                    Provider.of<GorevProvider>(context, listen: false);
                provider.gorevSil(widget.gorev);

                Navigator.of(context).pop();
              },
            )
          ],
        ),
        body: Padding(
          padding: EdgeInsets.all(16),
          child: Form(
            key: _formKey,
            child: GorevFormWidget(
              baslik: baslik,
              tanim: tanim,
              baslikDegismeEventi: (baslik) =>
                  setState(() => this.baslik = baslik),
              tanimDegismeEventi: (tanim) => setState(() => this.tanim = tanim),
              gorevKaydetEvent: () {
                {
                  final isValid = _formKey.currentState.validate();

                  if (!isValid) {
                    return;
                  } else {
                    final provider =
                        Provider.of<GorevProvider>(context, listen: false);

                    provider.gorevGuncelle(widget.gorev, baslik, tanim);

                    Navigator.of(context).pop();
                  }
                }
              },
            ),
          ),
        ),
      );
}

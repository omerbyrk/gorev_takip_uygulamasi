import 'package:flutter/cupertino.dart';
import 'package:gorev_takip_uygulamasi/yardimci.dart';

class GorevOzellikler {
  static const createdTime = 'olusturulmaZamani';
}

class Gorev {
  DateTime olusturulmaZamani;
  String baslik;
  String kimlik;
  String tanim;
  bool yapildiMi;

  Gorev({
    @required this.olusturulmaZamani,
    @required this.baslik,
    this.tanim = '',
    this.kimlik,
    this.yapildiMi = false,
  });

  static Gorev fromJson(Map<String, dynamic> json) => Gorev(
        olusturulmaZamani: Utils.toDateTime(json['olusturulmaZamani']),
        baslik: json['baslik'],
        tanim: json['tanim'],
        kimlik: json['kimlik'],
        yapildiMi: json['yapildiMi'],
      );

  Map<String, dynamic> toJson() => {
        'olusturulmaZamani': Utils.fromDateTimeToJson(olusturulmaZamani),
        'baslik': baslik,
        'tanim': tanim,
        'kimlik': kimlik,
        'yapildiMi': yapildiMi,
      };
}

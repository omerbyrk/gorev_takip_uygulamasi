import 'package:flutter/material.dart';

class GorevFormWidget extends StatelessWidget {
  final String baslik;
  final String tanim;
  final ValueChanged<String> baslikDegismeEventi;
  final ValueChanged<String> tanimDegismeEventi;
  final VoidCallback gorevKaydetEvent;

  const GorevFormWidget({
    Key key,
    this.baslik = '',
    this.tanim = '',
    @required this.baslikDegismeEventi,
    @required this.tanimDegismeEventi,
    @required this.gorevKaydetEvent,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            baslikPart(),
            SizedBox(height: 8),
            tanimPart(),
            SizedBox(height: 16),
            buttonPart(),
          ],
        ),
      );

  Widget baslikPart() => TextFormField(
        maxLines: 1,
        initialValue: baslik,
        onChanged: baslikDegismeEventi,
        validator: (baslik) {
          if (baslik.isEmpty) {
            return 'The title cannot be empty';
          }
          return null;
        },
        decoration: InputDecoration(
          border: UnderlineInputBorder(),
          labelText: 'Başlık',
        ),
      );

  Widget tanimPart() => TextFormField(
        maxLines: 3,
        initialValue: tanim,
        onChanged: tanimDegismeEventi,
        decoration: InputDecoration(
          border: UnderlineInputBorder(),
          labelText: 'Açıklama',
        ),
      );

  Widget buttonPart() => SizedBox(
        width: double.infinity,
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(Colors.blue),
          ),
          onPressed: gorevKaydetEvent,
          child: Text('Kaydet'),
        ),
      );
}

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:gorev_takip_uygulamasi/model/gorev.dart';
import 'package:gorev_takip_uygulamasi/provider/gorev_provider.dart';
import 'package:gorev_takip_uygulamasi/widget/gorev_form_widget.dart';

class GorevDiyalogEkleWidget extends StatefulWidget {
  @override
  _GorevDiyalogEkleWidgetState createState() => _GorevDiyalogEkleWidgetState();
}

class _GorevDiyalogEkleWidgetState extends State<GorevDiyalogEkleWidget> {
  final _formKey = GlobalKey<FormState>();
  String baslik = '';
  String tanim = '';

  @override
  Widget build(BuildContext context) => AlertDialog(
        content: Form(
          key: _formKey,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Görev Ekle',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 22,
                ),
              ),
              const SizedBox(height: 8),
              GorevFormWidget(
                baslikDegismeEventi: (title) =>
                    setState(() => this.baslik = title),
                tanimDegismeEventi: (description) =>
                    setState(() => this.tanim = description),
                gorevKaydetEvent: () {
                  final veriGecerliMi = _formKey.currentState.validate();

                  if (!veriGecerliMi) {
                    return;
                  } else {
                    final gorev = Gorev(
                      baslik: baslik,
                      tanim: tanim,
                      kimlik: DateTime.now().toString(),
                      olusturulmaZamani: DateTime.now(),
                    );

                    final provider =
                        Provider.of<GorevProvider>(context, listen: false);
                    provider.gorevEkle(gorev);

                    Navigator.of(context).pop();
                  }
                },
              ),
            ],
          ),
        ),
      );
}

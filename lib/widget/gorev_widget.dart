import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:provider/provider.dart';
import 'package:gorev_takip_uygulamasi/model/gorev.dart';
import 'package:gorev_takip_uygulamasi/page/gorev_guncelleme.dart';
import 'package:gorev_takip_uygulamasi/provider/gorev_provider.dart';
import 'package:gorev_takip_uygulamasi/yardimci.dart';

class GorevWidget extends StatelessWidget {
  final Gorev gorev;

  const GorevWidget({
    @required this.gorev,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => ClipRRect(
        borderRadius: BorderRadius.circular(16),
        child: Slidable(
          actionPane: SlidableDrawerActionPane(),
          key: Key(gorev.kimlik),
          actions: [
            IconSlideAction(
              color: Colors.green,
              onTap: () => Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => GorevGuncellemePage(gorev: gorev),
              )),
              caption: 'Güncelle',
              icon: Icons.edit,
            )
          ],
          secondaryActions: [
            IconSlideAction(
              color: Colors.red,
              caption: 'Sil',
              onTap: () {
                final provider =
                    Provider.of<GorevProvider>(context, listen: false);
                provider.gorevSil(gorev);

                Utils.showSnackBar(context, 'Görev silindi');
              },
              icon: Icons.delete,
            )
          ],
          child: gorevPart(context),
        ),
      );

  Widget gorevPart(BuildContext context) => GestureDetector(
        onTap: () => Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => GorevGuncellemePage(gorev: gorev),
        )),
        child: Container(
          color: Colors.white,
          padding: EdgeInsets.all(20),
          child: Row(
            children: [
              Checkbox(
                activeColor: Theme.of(context).primaryColor,
                checkColor: Colors.white,
                value: gorev.yapildiMi,
                onChanged: (_) {
                  final provider =
                      Provider.of<GorevProvider>(context, listen: false);
                  final isDone = provider.gorevDurumDegistir(gorev);

                  Utils.showSnackBar(
                    context,
                    isDone
                        ? 'Görev tamamlandı'
                        : 'Görev tamamlanmadı olarak işaretlendi',
                  );
                },
              ),
              const SizedBox(width: 20),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      gorev.baslik,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).primaryColor,
                        fontSize: 22,
                      ),
                    ),
                    if (gorev.tanim.isNotEmpty)
                      Container(
                        margin: EdgeInsets.only(top: 4),
                        child: Text(
                          gorev.tanim,
                          style: TextStyle(fontSize: 20, height: 1.5),
                        ),
                      )
                  ],
                ),
              ),
            ],
          ),
        ),
      );
}

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:gorev_takip_uygulamasi/provider/gorev_provider.dart';
import 'package:gorev_takip_uygulamasi/widget/gorev_widget.dart';

class YapilanGorevListesiWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<GorevProvider>(context);
    final gorevler = provider.yapilanGorevler;

    return gorevler.isEmpty
        ? Center(
            child: Text(
              'Görev bulunmamaktadır.',
              style: TextStyle(fontSize: 20),
            ),
          )
        : ListView.separated(
            physics: BouncingScrollPhysics(),
            padding: EdgeInsets.all(16),
            separatorBuilder: (context, index) => Container(height: 8),
            itemCount: gorevler.length,
            itemBuilder: (context, index) {
              final gorev = gorevler[index];

              return GorevWidget(gorev: gorev);
            },
          );
  }
}
